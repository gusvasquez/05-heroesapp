import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Auth } from '../interfaces/auth.interfaces';
import { tap, Observable, map, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl: string = environment.baseUrl;
  private _auth: Auth | undefined;

  get auth(): Auth{
    // se hace  {...this._auth} por que el no va a cambiar el valor que exista en el _auth
    return {...this._auth!};
  }

  constructor(private _htpp: HttpClient) { }

  // verificar la authenticacion
  verificarAuthenticacion(): Observable<boolean>{
    if(!localStorage.getItem('token')){
      return of(false);
    }

    return this._htpp.get<Auth>(`${this.baseUrl}/usuarios/1`).pipe(
      // map nos sirve para trasnformar y retornar un nuevo valor
      map(auth => {
        this._auth = auth;
        return true;
      })
    );
  }

  login(){
    return this._htpp.get<Auth>(`${this.baseUrl}/usuarios/1`)
    // antes de pase por el susbcribe pase por el tap
    .pipe(tap(response => this._auth = response),
    tap(response => localStorage.setItem('token', JSON.stringify(response.id))));
  }
}
