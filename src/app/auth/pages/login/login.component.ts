import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _router: Router,
    private _authService: AuthService) { }

  ngOnInit(): void {
  }

  login(){
    // ir al backend
    // un usuario
    this._authService.login().subscribe(
      response =>{
        console.log(response);
        if(response.id){
          this._router.navigate(['/heroes/listado']);
        }
      }
    );
    //
  }

}
