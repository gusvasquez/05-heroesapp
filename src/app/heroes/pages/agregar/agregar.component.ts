import { Component, OnInit } from '@angular/core';
import { Heroe, Publisher } from '../../interfaces/heroes.interface';
import { HeroesService } from '../../services/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmanComponent } from '../../components/confirman/confirman.component';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css'],
})
export class AgregarComponent implements OnInit {
  public publishers = [
    {
      id: 'DC Comics',
      desc: 'DC - Comics',
    },
    {
      id: 'Marvel Comics',
      desc: 'Marvel - Comics',
    },
  ];

  public heroe: Heroe = {
    superhero: '',
    alter_ego: '',
    characters: '',
    first_appearance: '',
    publisher: Publisher.DCComics,
    alt_img: '',
  };

  constructor(
    private _heroeService: HeroesService,
    private _activateRoute: ActivatedRoute,
    private _router: Router,
    private _snacckBar: MatSnackBar,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    // si la ruta no incluye con el nombre editar entonces no haga nada
    if (!this._router.url.includes('editar')) {
      return;
    }
    this._activateRoute.params
      .pipe(switchMap(({ id }) => this._heroeService.getHeroe(id)))
      .subscribe((response) => (this.heroe = response));
  }

  guardar() {
    if (this.heroe.superhero.trim().length === 0) {
      return;
    }

    if (this.heroe.id) {
      // actualizar
      this._heroeService.actualizarHeroe(this.heroe).subscribe((response) => {
        this.mostarAlerta('Registro Actualizado');
      });
    } else {
      // crear
      this._heroeService.agregarHeroe(this.heroe).subscribe((response) => {
        this._router.navigate(['/heroes/listado']);
        this.mostarAlerta('Registro Creado');
      });
    }
  }

  borrar() {
    // dialogo
    // el dialogo reciber un componente
    const dialog = this.dialog.open(ConfirmanComponent, {
      width: '250px',
      data: this.heroe,
    });
    // para validar la respuesta del dialog
    dialog.afterClosed().subscribe((response) => {
      if (response) {
        // se pone asi this.heroe.id! para tyscript confie que siempre hay un valor
        this._heroeService.borrarHeroe(this.heroe.id!).subscribe((response) => {
          this._router.navigate(['/heroes/listado']);
          this.mostarAlerta('Heroe eliminado');
        });
      }
    });
  }

  mostarAlerta(mensaje: string) {
    this._snacckBar.open(mensaje, 'ok!', {
      duration: 2500,
    });
  }
}
