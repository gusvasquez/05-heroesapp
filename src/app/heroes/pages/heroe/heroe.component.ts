import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Heroe } from '../../interfaces/heroes.interface';
import { HeroesService } from '../../services/heroes.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css'],
})
export class HeroeComponent implements OnInit {
  public heroe!: Heroe;

  constructor(
    private activateRoute: ActivatedRoute,
    private _heroService: HeroesService,
    private _router: Router
  ) {}

  ngOnInit(): void {

    this.activateRoute.params
      .pipe(switchMap(({ id }) => this._heroService.getHeroe(id)))
      .subscribe((response) => {
        this.heroe = response;
      });

    // se puede hacer asi tambien
    //this.activateRoute.params.subscribe(({ id }) => {
    //  this._heroService.getHeroe(id).subscribe(
    //    response => {
    //      this.heroe = response;
    //      console.log(this.heroe);
    //    }
    //  );
    //});
  }

  regresar(){
    this._router.navigate(['/heroes/listado']);
  }
}
