import { Component, OnInit } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Heroe } from '../../interfaces/heroes.interface';
import { HeroesService } from '../../services/heroes.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnInit {

  public termino:string ='';
  public heroes: Heroe[]= [];
  // se pone si hay datos que sea de tipo heroe y si no hay datos que sea undefined
  public heroeSelecionado: Heroe | undefined;

  constructor(private _heroeService: HeroesService) { }

  ngOnInit(): void {
  }

  buscar(){
    this._heroeService.getSugerencias(this.termino.trim()).subscribe(
      response =>{
          this.heroes = response;
      }
    );
  }

  opcionSeleccionada(evento: MatAutocompleteSelectedEvent ){
    // se valida si no hay datos en el input y para que no devuelva error
    if(!evento.option.value){
      // se valida si no hay datos en el heroe selecionado y si no hay se pone undefined
      this.heroeSelecionado = undefined;
      return;
    }
    const heroe: Heroe = evento.option.value;
    this.termino = heroe.superhero;
    this._heroeService.getHeroe(heroe.id!).subscribe(
      response =>{
         this.heroeSelecionado = response;
      }
    );
  }

}
