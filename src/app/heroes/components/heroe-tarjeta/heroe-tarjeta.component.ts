import { Component, Input, OnInit } from '@angular/core';
import { Heroe } from '../../interfaces/heroes.interface';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html',
  styleUrls: ['./heroe-tarjeta.component.css']
})
export class HeroeTarjetaComponent implements OnInit {

  // input nos recibe el la variable data que se pone en el componente de lsitado.component
  // se pone ! para que angular confie que siempre va a recibir datos en la variable data
  @Input() data!: Heroe;

  constructor() { }

  ngOnInit(): void {
  }

}
