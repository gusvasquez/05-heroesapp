import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Heroe } from '../interfaces/heroes.interface';
// tener en cuenta cuando se importe el environment e importat el correo y no el de produccion
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class HeroesService {
  private basUrl: string = environment.baseUrl;

  constructor(private _http: HttpClient) {}

  getHeroes(): Observable<Heroe[]> {
    return this._http.get<Heroe[]>(`${this.basUrl}/heroes`);
  }

  getHeroe(id: string): Observable<Heroe> {
    return this._http.get<Heroe>(`${this.basUrl}/heroes/${id}`);
  }

  getSugerencias(termino: string): Observable<Heroe[]> {
    return this._http.get<Heroe[]>(
      `${this.basUrl}/heroes?q=${termino}&_limit=5`
    );
  }

  agregarHeroe(heroe: Heroe): Observable<Heroe>{
    return this._http.post<Heroe>(`${this.basUrl}/heroes/`, heroe);
  }

  actualizarHeroe(heroe: Heroe): Observable<Heroe>{
    return this._http.put<Heroe>(`${this.basUrl}/heroes/${heroe.id}`, heroe);
  }

  borrarHeroe(id: string): Observable<any>{
    return this._http.delete<any>(`${this.basUrl}/heroes/${id}`);
  }
}
